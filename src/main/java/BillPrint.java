import javax.swing.plaf.nimbus.State;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */


/**
 * refactoring a long function:
 * separate different parts
 * variable name include type
 * no need to pass in params that can be computed by existing params
 * to remove volumeCredits
 * 		Split Loop (227) to isolate the accumulation
 * 		Slide Statements (223) to bring the initializing code next to the accumulation
 * 		Extract Function (106) to create a function for calculating the total
 * 		Inline Variable (123) to remove the variable completely
 * split phase: calculate data + render HTML
 * */


public class BillPrint {
	private HashMap<String, Play> plays;
	private String customer;
	private ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap();
		for (Play p: plays) { this.plays.put(p.getId(),p); }

		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}


	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

//	play can be derived from aPerformance
	public int amountFor(Performance aPerformance){
		int result = 0;

		switch (aPerformance.getPlay().getType()) {
			case "tragedy": result = 40000;
				if (aPerformance.getAudience() > 30) {
					result += 1000 * (aPerformance.getAudience() - 30);
				}
				break;
			case "comedy":  result = 30000;
				if (aPerformance.getAudience() > 20) {
					result += 10000 + 500 * (aPerformance.getAudience() - 20);
				}
				result += 300 * aPerformance.getAudience();
				break;
			default:        throw new IllegalArgumentException("unknown type: " +  aPerformance.getPlay().getType());
		}
		return result;
	}


	public Play playFor(Performance aPerformance){
		return plays.get(aPerformance.getPlayID());
	}

	public void enrichPerformance(Performance performance){
		performance.setPlay(playFor(performance));
		performance.setAmount(amountFor(performance));
		performance.setVolumeCredits(volumeCreditsFor(performance));
	}

	public int volumeCreditsFor(Performance aPerformance){
		int result = 0;
		result += Math.max(aPerformance.getAudience() - 30, 0);
		if ( aPerformance.getPlay().getType().equals("comedy")) {
			result += Math.floor((double) aPerformance.getAudience() / 5.0);
		}
		return result;
	}

	public String renderHtml (StatementData data) {
		String result = String.format("<h1>Statement for %s</h1>\n", data.customer);
		result += "<table>\n";
		result += "<tr><th>play</th><th>seats</th><th>cost</th></tr>";
		for (Performance perf: data.performances) {
			result += String.format("<tr><td>%s</td><td>%s</td>",
					perf.getPlay().getName(), perf.getAudience());
			result += String.format("<td>%d</td></tr>\n", usd(perf.getAmount()));
		}
		result += "</table>\n";
		result += String.format("<p>Amount owed is <em>%d</em></p>\n", usd(data.getTotalAmount()));
		result += String.format("<p>You earned <em>%d</em> credits</p>\n",
				data.getTotalVolumeCredits());
		return result;
	}

	public String htmlStatement () {
		return renderHtml(createStatementData());
	}

	public String usd(int nCents){
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return "$" + numberFormat.format(nCents / 100.00);
	}

	public int totalVolumeCredits(StatementData data){
		int result = 0;
		for (Performance perf: data.performances) {
			// add volume credits
			result += perf.getVolumeCredits();
		}
		return result;
	}

	public int totalAmount(StatementData data){
		int result = 0;
		for (Performance perf: data.performances){
			result += perf.getAmount();
		}
		return result;
	}

	public String renderPlainText(StatementData data){
		String result = "Statement for " + data.customer + "\n";
		for (Performance perf: data.performances) {
			result += "  " + perf.getPlay().getName() + ": " + usd(perf.getAmount()) + " (" + perf.getAudience()
					+ " seats)" + "\n";
		}
		result += "Amount owed is " + usd(data.getTotalAmount()) + "\n";
		result += "You earned " + data.getTotalVolumeCredits() + " credits" + "\n";
		return result;
	}

	public StatementData createStatementData(){
		StatementData statementData = new StatementData(customer, performances);
		for (Performance perf: statementData.performances){
			enrichPerformance(perf);
		}
		statementData.setTotalAmount(totalAmount(statementData));
		statementData.setTotalVolumeCredits(totalVolumeCredits(statementData));
		return statementData;
	}

	public String statement() {
		return renderPlainText(createStatementData());
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<Play>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<Performance>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
