import java.util.ArrayList;

public class StatementData {
    public String customer;
    public ArrayList<Performance> performances;
    public int totalAmount;
    public int totalVolumeCredits;

    public StatementData(){};

    public StatementData(String customer, ArrayList<Performance> performances) {
        this.customer = customer;
        this.performances = performances;
    };

    public void setCustomer(String customer){
        this.customer = customer;
    }

    public void setPerformances(ArrayList<Performance> performances){
        this.performances = performances;
    }

    public void setTotalAmount(int totalAmount){
        this.totalAmount = totalAmount;
    }

    public int getTotalAmount(){
        return this.totalAmount;
    }

    public void setTotalVolumeCredits(int totalVolumeCredits){
        this.totalVolumeCredits = totalVolumeCredits;
    }

    public int getTotalVolumeCredits(){
        return this.totalVolumeCredits;
    }
}
